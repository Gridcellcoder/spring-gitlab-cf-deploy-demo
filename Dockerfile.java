FROM base:base

WORKDIR /app/tools

RUN wget https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u332-b09/OpenJDK8U-jdk_x64_alpine-linux_hotspot_8u332b09.tar.gz
RUN tar xvf OpenJDK8U-jdk_x64_alpine-linux_hotspot_8u332b09.tar.gz

ENV JAVA_HOME=/app/tools/jdk8u332-b09
ENV PATH=$JAVA_HOME/bin:$PATH

